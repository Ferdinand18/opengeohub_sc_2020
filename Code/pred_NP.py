#!/usr/bin/env python
# coding: utf-8

# # <center>NP Modelling</center>
# <p>
# ## <center>Longzhu Shen</center> 
# <p>
# ## <center><i>Spatial Ecology</i></center>
# <p>
# ## <center>Aug 2020</center>

# ## BackGround 
# - Geoenviornmental variables 
# - Ground observationd : Nitrogen in US streams  

# ## Code

# In[1]:


import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor as RFReg
from sklearn.model_selection import train_test_split,GridSearchCV
from sklearn.pipeline import Pipeline
from scipy.stats.stats import pearsonr
from scipy.stats import boxcox
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (10,6.5)


# read in data

# In[2]:


dsIn = pd.read_csv("TN_Data_Sample.csv")
dsIn.head(6)


# In[158]:


dsIn.columns.values


# In[3]:


bins = np.linspace(min(dsIn['mean']),max(dsIn['mean']),100)
plt.hist((dsIn['mean']),bins,alpha=0.8);


# In[4]:


dsIn['bc'], lambdabox =  boxcox(dsIn['mean'])


# In[5]:


bins = np.linspace(min(dsIn['bc']),max(dsIn['bc']),100)
plt.hist((dsIn['bc']),bins,alpha=0.8);


# In[7]:


X = dsIn.iloc[:,3:50].values
Y = dsIn.iloc[:,50:51].values
feat = dsIn.iloc[:,3:50].columns.values


# In[11]:


feat


# In[164]:


X.shape


# In[8]:


Y.shape

model building
# In[9]:


X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.5, random_state=24)
y_train = np.ravel(Y_train)
y_test = np.ravel(Y_test)


# In[25]:


pipeline = Pipeline([('rf',RFReg())])

parameters = {
        'rf__max_features':("log2","sqrt",0.33),
        'rf__max_samples':(0.5,0.6,0.7),
        'rf__n_estimators':(500,1000,2000),
        'rf__max_depth':(50,100,200)}

grid_search = GridSearchCV(pipeline,parameters,n_jobs=-1,cv=3,scoring='r2',verbose=1)
grid_search.fit(X_train,y_train)


# In[41]:


print ('Best Training score: %0.3f' % grid_search.best_score_)
print ('Optimal parameters:')
best_par = grid_search.best_estimator_.get_params()
for par_name in sorted(parameters.keys()):
    print ('\t%s: %r' % (par_name, best_par[par_name]))


# In[10]:


rfReg = RFReg(n_estimators=500,max_features=0.33,max_depth=200,max_samples=0.7,n_jobs=-1,random_state=24)
rfReg.fit(X_train, y_train);
dic_pred = {}
dic_pred['train'] = rfReg.predict(X_train)
dic_pred['test'] = rfReg.predict(X_test)
[pearsonr(dic_pred['train'],y_train)[0],pearsonr(dic_pred['test'],y_test)[0]]


# In[12]:


plt.scatter(y_train,dic_pred['train'])
plt.xlabel('orig')
plt.ylabel('pred')
ident = [-1.5, 2]
plt.plot(ident,ident,'r--')


# In[14]:


plt.scatter(y_test,dic_pred['test'])
plt.xlabel('orig')
plt.ylabel('pred')
ident = [-2, 2]
plt.plot(ident,ident,'r--')


# In[15]:


impt = [rfReg.feature_importances_, np.std([tree.feature_importances_ for tree in rfReg.estimators_],axis=1)] 
ind = np.argsort(impt[0])


# In[16]:


plt.rcParams["figure.figsize"] = (6,12)
plt.barh(range(len(feat)),impt[0][ind],color="b", xerr=impt[1][ind], align="center")
plt.yticks(range(len(feat)),feat[ind]);

